# Web Thing Examples
These examples show you how to build your own Web Things.

You can use these with the [Mozilla Things Gateway](https://iot.mozilla.org/gateway/).

## First steps: properties
Let’s start with something simple: A Web Thing with one read-only property.

[Light Schedule Thing](01-LightScheduleThing/ReadMe.md)
