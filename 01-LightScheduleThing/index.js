const express = require('express');
const app = express();

const scheduler = {
    name: 'Light Scheduler',
    type: 'thing',
    description: 'Schedules the light',
    properties: {
        'should-be-on': {
            label: 'Should be on',
            type: 'boolean',
            description: 'Whether the lamp should be turned on',
            href: '/properties/should-be-on',
        },
    },
    actions: {},
    events: {},
    links: [
        {
            'rel': 'properties',
            'href': '/properties'
        },
        {
            'rel': 'actions',
            'href': '/actions'
        },
        {
            'rel': 'events',
            'href': '/events'
        },
    ],
};

const SUNDAY = 0;
const MONDAY = 1;
const TUESDAY = 2;
const WEDNESDAY = 3;
const THURSDAY = 4;
const FRIDAY = 5;
const SATURDAY = 6;


function shouldBeOn() {
    const now = new Date();
    const isWeekday = [MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY].includes(now.getDay());
    const isMorning =
        (now.getHours() === 6 && now.getMinutes() >= 30)
        || (now.getHours() === 7 && now.getMinutes() < 30);
    return isWeekday && isMorning;
}

app.all('*', function (req, res, next) {
    console.log(req.url);
    next(); // Hand it off to the next handler
});

app.get('/', function (req, res) {
    res.json(scheduler);
});

app.get('/properties', function (req, res) {
    res.json({
        'should-be-on': shouldBeOn(),
    });
});

app.get('/properties/should-be-on', function (req, res) {
    res.json({
        'should-be-on': shouldBeOn(),
    });
});

app.get('/actions', function (req, res) {
    res.json([]);
});

app.get('/events', function (req, res) {
    res.json([]);
});

app.listen(3000);
