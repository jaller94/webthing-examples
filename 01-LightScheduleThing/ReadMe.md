# Light Schedule thing
This Web Thing is a first introduction to building your own Web Thing. It has only one read-only property and no actions or events.

## Goal
The Light Scheduler has one boolean property indicating whether it is 6:30 AM to 7:30 AM on a weekday. If I use this as an input in a rule to turn on my light bulb, the light is forced to be on during this time period – hopefully waking me up gently.

### v0.6 and Above
For those testing on __Things Gateway v0.6 and above__, the *Add by URL* option is unavailabe natively in the Add Things page. To get it, the Web Thing add-on should be added and enabled.

To enable to it go to Settings > Add-ons and do a browser search for Web Things. Upon adding it, retry the Add Things option, it should be available now.
